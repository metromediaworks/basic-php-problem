# Basic PHP Problem: Task List API

Your goal is to write a simple task list API using a custom class or library that has two methods: create and delete. 

Below you fill find a list of requirements for the API:

 - The API should accept POST requests to add a task and DELETE requests to delete a task.
 - Tasks should have a name, description, and timestamp, accepting name and description as parameters in the create method.
 - Duplicate task names should not be allowed.
 - Tasks should be stored in and removed from tasks.json (flat file).
 
What we hope to see in your solution:

 - OOP.
 - Simplicity and flexibility.
 
_Please create a pull request with your code when you have completed this problem set._
 
### Please provide any notes or documentation you feel is necessary below:

```
// Docs or notes here
```